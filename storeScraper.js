const puppeteer = require('puppeteer');

const self = {
    browser: null,
    pages: null,
    config: {
        client: '',
        url: '',
        container: '',
        containerSelector: '',
        productSelector: '',
        productChildrenSelectors: [],
        paginationSelector: ''
    },

    init: async (configs) => {
        await self.parseConfigs(configs);

        self.browser = await puppeteer.launch({headless: false});
        self.page = await self.browser.newPage();

        await self.page.goto(self.config.url, {waitUntilt: 'networkidle0'});
    },

    getResults: async (numberResults) => {
        let results = [];

        do {
            let newResults = await self.parseResults();

            results = [...results, ...newResults];

            if (results.length < numberResults) {
                const nextPageButton = await self.page.$(self.config.paginationSelector);

                if (nextPageButton) {
                    await nextPageButton.click();
                    await self.page.waitForNavigation({waitUntilt: 'networkidle0'});
                } else {
                    break;
                }
            }
        } while (results.length < numberResults);

        return {client: self.config.client, products: results.slice(0, numberResults)};
    },

    parseResults: async () => {
        const elements = await self.page.$$(self.config.containerSelector + ' ' + self.config.productSelector);

        let results = [];
        for (let element of elements) {
            let product = {};
            for (const children of self.config.productChildrenSelectors) {
                const prop = Object.keys(children)[0];

                if (prop === 'title') {
                    product[prop] = await element.$eval((children[prop]), node => node.innerText.trim());
                } else if (prop === 'price') {
                    product[prop] = await element.$eval((children[prop]), node => +node.innerText
                        .replace('€', '')
                        .replace(',', '.')
                        .trim());
                }
            }

            results.push(product);
        }

        return results;
    },

    parseConfigs: async (configs) => {
        self.config.client = configs._id;
        self.config.url = configs.startUrl[0];

        for (const selector of configs.selectors) {
            if (selector.parentSelectors.includes('_root')) {
                if (selector.id === 'pagination') {
                    self.config.paginationSelector = selector.selector;
                } else {
                    self.config.container = selector.id;
                    self.config.containerSelector = selector.selector;
                }
            } else if (selector.parentSelectors.includes(self.config.container) && selector.multiple) {
                self.config.productSelector = selector.selector;
            } else {
                let prop = selector.id;
                self.config.productChildrenSelectors.push({[prop]: selector.selector});
            }
        }
    }
};

module.exports = self;