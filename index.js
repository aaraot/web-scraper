const storeScraper = require('./storeScraper');

(async () => {
    const configs = {
        "_id": "worten_pt",
        "startUrl": [
            "https://www.worten.pt/informatica-e-acessorios/computadores/computadores-portateis"
        ],
        "selectors": [
            {
                "id": "products_list",
                "type": "SelectorElement",
                "parentSelectors": [
                    "_root"
                ],
                "selector": "div.w-product-list__row",
                "multiple": false,
                "delay": 0
            },
            {
                "id": "product",
                "type": "SelectorElement",
                "parentSelectors": [
                    "products_list"
                ],
                "selector": "div.w-product__wrapper",
                "multiple": true,
                "delay": 0
            },
            {
                "id": "title",
                "type": "SelectorText",
                "parentSelectors": [
                    "product"
                ],
                "selector": "h3",
                "multiple": false,
                "regex": "",
                "delay": 0
            },
            {
                "id": "price",
                "type": "SelectorText",
                "parentSelectors": [
                    "product"
                ],
                "selector": "span.w-currentPrice",
                "multiple": false,
                "regex": "",
                "delay": 0
            },
            {
                "id": "pagination",
                "type": "SelectorElement",
                "parentSelectors": [
                    "_root"
                ],
                "selector": ".pagination-next a",
                "multiple": false,
                "delay": 0
            }
        ]
    };

    await storeScraper.init(configs);

    let results = await storeScraper.getResults(48);

    console.log(results);
})();